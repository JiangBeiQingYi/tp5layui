<?php

namespace app\common\controller;
// +----------------------------------------------------------------------
// | 后台基础控制器
// +----------------------------------------------------------------------
// | Author:  Eric <ieyangxiaoguo@126.com>
// +----------------------------------------------------------------------
// | Copyright ©2018  http://www.zzuyxg.top  All rights reserved.
// +----------------------------------------------------------------------
// | 2018年3月22日
// +----------------------------------------------------------------------

use think\Db;
use think\Controller;
use auth\Auth;

class AdminBaseController extends Controller
{
    public function initialize()
    {
        $auth = new Auth();
        $ajaxFlag=$this->request->isAjax();
        //dump($ajaxFlag);
        parent::initialize();
        $session_admin_account = session('sysuser.account');
        if (empty($session_admin_account)) {
            if ($this->request->isPost()) {
                header("Location:" . url("admin/public/index"));
                // 				$this->error("您还没有登录！", url("admin/public/login"));
            } else {
                header("Location:" . url("admin/public/index"));
            }
            exit();
        } else {
            $pathinfo = $this->request->path();
            if ($session_admin_account != "admin") {//如果不是Administrator，根据权限组ID进行验证
                if (!$auth->check($pathinfo, session('sysuser.id'))) {
                    if($ajaxFlag){
                        $this->error("您没有访问权限！");
                    }else{
                        $this->error("您没有访问权限！");
                        echo("您没有访问权限！");
                    }
                    exit();
                }
            }
        }

        $this->assign("menulist",erwei(session("sysuser.sysmenu")));
    }
}

