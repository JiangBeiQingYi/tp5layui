<?php

namespace app\admin\model;

use think\model\Pivot;

class SysroleMenuModel extends Pivot
{
    protected $autoWriteTimestamp = false;
}
