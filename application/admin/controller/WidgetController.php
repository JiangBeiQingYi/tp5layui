<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use app\admin\model\SysroleModel;
use app\admin\model\SysmenuModel;

class WidgetController extends Controller
{
    public function widgetUserMenuList($userid = 0)
    {
        $usermenulist = Db::name("sysmenu")->alias("m ")->join("sysrole_menu rm ", ["rm.menu_id=m.id"])
            ->join("sysrole_user ru", [" ru.role_id=rm.role_id",])
            ->where("ru.user_id='$userid'")->select();

        $json = [
            "data"   => $usermenulist,
            "status" => [
                "code"    => 200,
                "message" => "成功",
            ],
        ];
        return json($usermenulist);
    }

    public function widgetRoleMenuList($roleid = 0)
    {
        $menulist = Db::name("sysmenu")->alias("m ")->join("sysrole_menu rm ", ["rm.menu_id=m.id"])
            ->where("rm.role_id='$roleid'")->field('m.id,m.title,m.name')->select();

        $json = [
            "data"   => $menulist,
            "status" => [
                "code"    => 200,
                "message" => "成功",
            ],
        ];
        return ($menulist);
    }

    public function widgetRoleList()
    {
        $sysrolemodel = new SysroleModel();
        return $sysrolemodel->where("status", 1)->select();
    }

    public function widgetUserRoleList($userid = 0)
    {

        $sysrolemodel = new SysroleModel();
        return $sysrolemodel->join("sysrole_user ru ", ["sysrole.id=ru.role_id"])->where("ru.user_id=" . $userid)->select();
    }

    public function widgetMenu()
    {
        $sysmenu = new SysmenuModel();
        $sysmenulist = $sysmenu->select();
        $sysmenulist = tree($sysmenulist);
        $json = [
            "data"   => $sysmenulist,
            "status" => [
                "code"    => 200,
                "message" => "成功",
            ],
        ];
        return json($json);
    }
}