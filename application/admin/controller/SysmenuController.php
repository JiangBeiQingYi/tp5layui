<?php

namespace app\admin\controller;

use think\Db;
use think\facade\View;
use app\admin\model\SysuserModel;
use app\admin\model\SysmenuModel;
use app\common\controller\AdminBaseController;
use app\admin\validate\SysmenuValidate;

class SysmenuController extends AdminBaseController
{
    public function index()
    {
        //$sysmenu=SysuserModel::all();
        //dump($sysmenu);

        return View::fetch();
    }

    public function view()
    {
        $sysmenu = new SysmenuModel();
        $sysmenulist = $sysmenu->select();
        $json = [
            "data" => $sysmenulist,
        ];
        return json($json);
    }

    public function add()
    {
        $isPost = $this->request->isPost();
        if ($isPost) {
            $params = input('post.');

            $validate=new SysmenuValidate();
            if (!$validate->check($params)) {
                $this->error($validate->getError());
                exit;
            }

            try {
                $res = Db::name('sysmenu')->insert($params);
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success("成功！", "", $params);
        } else {
            $sysmenuModel = new SysmenuModel();
            $sysmenulist = $sysmenuModel->select();
            $this->assign("nodelist", $sysmenulist);
            return View::fetch();
        }
    }

    public function edit()
    {
        $isPost = $this->request->isPost();
        if ($isPost) {
            $params = input('post.');
            Db::name('sysmenu')->update($params);
            $this->success("成功！", "", $params);
        } else {
            $menuid = $this->request->param("id");
            $sysmenuModel = new SysmenuModel();
            $sysmenu = $sysmenuModel->find($menuid);
            //dump($sysmenu);
            $this->assign("sysMenu", $sysmenu);

            $sysmenulist = $sysmenuModel->select();
            $this->assign("nodelist", $sysmenulist);
            return View::fetch();
        }
    }

    public function delete()
    {
        $id = input('param.id');
        $sysmenu = new SysmenuModel();
        $res = $sysmenu::destroy($id);
        $this->success("成功！", "", $res);
    }
}